# React Native Nem Sdk

## Installing

Install the sdk

```
npm i @dbxsoapp/react-native-nem-sdk
```

## Examples

Set your private key to your transaction

```
import nem from "@dbxsoapp/react-native-nem-sdk";

nem.setPrivateKey(PRIVATE_KEY);
```

Get Hash Key for Transaction

```
const hashKey = nem.send(
          RECEIVER_ADDRESS,
          TRANSFER_AMOUNT,
          MESSAGE,
          ENVIRONMENT === "production"
            ? 104
            : -104
        );
```

Note: For aditional reference please check the scroce package [NEM sdk](https://github.com/QuantumMechanics/NEM-sdk) & [@coincrowd/react-native-nem-sdk](https://npm.io/package/@coincrowd/react-native-nem-sdk)

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _convert = require('./convert');

var _convert2 = _interopRequireDefault(_convert);

var _naclFast = require('./nacl-fast');

var _naclFast2 = _interopRequireDefault(_naclFast);

var _cryptoJs = require('crypto-js');

var _cryptoJs2 = _interopRequireDefault(_cryptoJs);

var _NetworkType = require('./NetworkType');

var _NetworkType2 = _interopRequireDefault(_NetworkType);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var currentFeeFactor = 0.05;
var alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';

var BinaryKey = function BinaryKey(keyData) {
  this.data = keyData;
  this.toString = function () {
    return _convert2.default.ua2hex(this.data);
  };
};

var hashfunc = function hashfunc(dest, data, dataLength) {
  var convertedData = _convert2.default.ua2words(data, dataLength);
  var hash = _cryptoJs2.default.SHA3(convertedData, {
    outputLength: 512
  });
  _convert2.default.words2ua(dest, hash);
};

var prepare = function prepare(publicKey, tx, network) {
  var actualSender = publicKey.toString();
  var recipientCompressedKey = tx.recipient.toString();
  var amount = Math.round(tx.amount * 1000000);
  var message = {
    'type': 1,
    'payload': _convert2.default.utf8ToHex(tx.message.toString())
  };
  var msgFee = calculateMessage(message);
  var due = network === _NetworkType2.default.testnetId ? 60 : 24 * 60;
  var entity = constructTx(actualSender, recipientCompressedKey, amount, message, msgFee, due, network);
  return entity;
};

var constructTx = function constructTx(senderPublicKey, recipientCompressedKey, amount, message, msgFee, due, network) {
  var timeStamp = createNEMTimeStamp();
  var version = getVersion(1, network);
  var transferType = 0x101;
  var data = txCommonPart(transferType, senderPublicKey, timeStamp, due, version);
  var fee = currentFeeFactor * calculateMinimum(amount / 1000000);
  var totalFee = Math.floor((msgFee + fee) * 1000000);
  var custom = {
    'recipient': recipientCompressedKey.toUpperCase().replace(/-/g, ''),
    'amount': amount,
    'fee': totalFee,
    'message': message
  };
  var entity = extendObj(data, custom);
  return entity;
};

var calculateMinimum = function calculateMinimum(numNem) {
  var fee = Math.floor(Math.max(1, numNem / 10000));
  return fee > 25 ? 25 : fee;
};

var extendObj = function extendObj() {
  for (var i = 1; i < arguments.length; i++) {
    for (var key in arguments[i]) {
      if (arguments[i].hasOwnProperty(key)) {
        arguments[0][key] = arguments[i][key];
      }
    }
  }
  return arguments[0];
};

var txCommonPart = function txCommonPart(txtype, senderPublicKey, timeStamp, due, version, network) {
  return {
    'type': txtype || '',
    'version': version || '',
    'signer': senderPublicKey || '',
    'timeStamp': timeStamp || '',
    'deadline': timeStamp + due * 60 || ''
  };
};

var getVersion = function getVersion(val, network) {
  if (network === _NetworkType2.default.mainnetId) {
    return 0x68000000 | val;
  } else if (network === _NetworkType2.default.testnetId) {
    return 0x98000000 | val;
  }
  return 0x60000000 | val;
};

var createNEMTimeStamp = function createNEMTimeStamp() {
  var NEM_EPOCH = Date.UTC(2015, 2, 29, 0, 6, 25, 0);
  return Math.floor(Date.now() / 1000 - NEM_EPOCH / 1000);
};

var calculateMessage = function calculateMessage(message) {
  if (!message.payload || !message.payload.length) {
    return 0.00;
  }

  var length = message.payload.length / 2;

  return currentFeeFactor * (Math.floor(length / 32) + 1);
};

var json = function json(data) {
  return {
    'Content-Type': 'application/json',
    'Content-Length': Buffer.from(data).byteLength
  };
};

// Signature
var sign = function sign(data, kp) {
  var sig = new Uint8Array(64);
  var hasher = new Hashobj();
  var r = _naclFast2.default.lowlevel.crypto_sign_hash(sig, kp, data, hasher);
  if (!r) {
    throw new Error("Couldn't sign the tx, generated invalid signature");
  }
  return new BinaryKey(sig);
};

/***
* Create an hasher object
*/
var Hashobj = function Hashobj() {
  this.sha3 = _cryptoJs2.default.algo.SHA3.create({
    outputLength: 512
  });
  this.reset = function () {
    this.sha3 = _cryptoJs2.default.algo.SHA3.create({
      outputLength: 512
    });
  };

  this.update = function (data) {
    if (data instanceof BinaryKey) {
      var converted = _convert2.default.ua2words(data.data, data.data.length);
      this.sha3.update(converted);
    } else if (data instanceof Uint8Array) {
      var _converted = _convert2.default.ua2words(data, data.length);
      this.sha3.update(_converted);
    } else if (typeof data === 'string') {
      var _converted2 = _cryptoJs2.default.enc.Hex.parse(data);
      this.sha3.update(_converted2);
    } else {
      throw new Error('unhandled argument');
    }
  };

  this.finalize = function (result) {
    var hash = this.sha3.finalize();
    _convert2.default.words2ua(result, hash);
  };
};

var b32encode = function b32encode(s) {
  var parts = [];
  var quanta = Math.floor(s.length / 5);
  var leftover = s.length % 5;

  if (leftover !== 0) {
    for (var i = 0; i < 5 - leftover; i++) {
      s += '\x00';
    }
    quanta += 1;
  }

  for (var _i = 0; _i < quanta; _i++) {
    parts.push(alphabet.charAt(s.charCodeAt(_i * 5) >> 3));
    parts.push(alphabet.charAt((s.charCodeAt(_i * 5) & 0x07) << 2 | s.charCodeAt(_i * 5 + 1) >> 6));
    parts.push(alphabet.charAt((s.charCodeAt(_i * 5 + 1) & 0x3F) >> 1));
    parts.push(alphabet.charAt((s.charCodeAt(_i * 5 + 1) & 0x01) << 4 | s.charCodeAt(_i * 5 + 2) >> 4));
    parts.push(alphabet.charAt((s.charCodeAt(_i * 5 + 2) & 0x0F) << 1 | s.charCodeAt(_i * 5 + 3) >> 7));
    parts.push(alphabet.charAt((s.charCodeAt(_i * 5 + 3) & 0x7F) >> 2));
    parts.push(alphabet.charAt((s.charCodeAt(_i * 5 + 3) & 0x03) << 3 | s.charCodeAt(_i * 5 + 4) >> 5));
    parts.push(alphabet.charAt(s.charCodeAt(_i * 5 + 4) & 0x1F));
  }

  var replace = 0;
  if (leftover === 1) replace = 6;else if (leftover === 2) replace = 4;else if (leftover === 3) replace = 3;else if (leftover === 4) replace = 1;

  for (var _i2 = 0; _i2 < replace; _i2++) {
    parts.pop();
  }for (var _i3 = 0; _i3 < replace; _i3++) {
    parts.push('=');
  }return parts.join('');
};

var b32decode = function b32decode(s) {
  var r = new ArrayBuffer(s.length * 5 / 8);
  var b = new Uint8Array(r);
  for (var j = 0; j < s.length / 8; j++) {
    var v = [0, 0, 0, 0, 0, 0, 0, 0];
    for (var _i4 = 0; _i4 < 8; ++_i4) {
      v[_i4] = alphabet.indexOf(s[j * 8 + _i4]);
    }
    var i = 0;
    b[j * 5 + 0] = v[i + 0] << 3 | v[i + 1] >> 2;
    b[j * 5 + 1] = (v[i + 1] & 0x3) << 6 | v[i + 2] << 1 | v[i + 3] >> 4;
    b[j * 5 + 2] = (v[i + 3] & 0xf) << 4 | v[i + 4] >> 1;
    b[j * 5 + 3] = (v[i + 4] & 0x1) << 7 | v[i + 5] << 2 | v[i + 6] >> 3;
    b[j * 5 + 4] = (v[i + 6] & 0x7) << 5 | v[i + 7];
  }
  return b;
};

var id2Prefix = function id2Prefix(id) {
  if (id === 104) {
    return '68';
  } else if (id === -104) {
    return '98';
  } else {
    return '60';
  }
};

var id2Char = function id2Char(id) {
  if (id === 104) {
    return 'N';
  } else if (id === -104) {
    return 'T';
  } else {
    return 'M';
  }
};

exports.default = {
  sign: sign,
  BinaryKey: BinaryKey,
  hashfunc: hashfunc,
  prepare: prepare,
  json: json,
  b32encode: b32encode,
  b32decode: b32decode,
  id2Prefix: id2Prefix,
  id2Char: id2Char
};
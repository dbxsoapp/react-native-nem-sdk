"use strict";

var _serialization = require("./serialization");

var _serialization2 = _interopRequireDefault(_serialization);

var _convert = require("./convert");

var _convert2 = _interopRequireDefault(_convert);

var _naclFast = require("./nacl-fast");

var _naclFast2 = _interopRequireDefault(_naclFast);

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

var _utils = require("./utils");

var _utils2 = _interopRequireDefault(_utils);

var _cryptoJs = require("crypto-js");

var _cryptoJs2 = _interopRequireDefault(_cryptoJs);

var _websockets = require("./websockets");

var _websockets2 = _interopRequireDefault(_websockets);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var send = function send(
  recipient,
  amount,
  message,
  networkId,
  onReceive,
  onError,
  onConfirm
) {
  var transferTransaction = {
    amount: amount || 0,
    recipient: recipient || "",
    recipientPublicKey: "",
    message: message || "",
    messageType: 1,
  };

  var publicKey = new _utils2.default.BinaryKey(
    new Uint8Array(_naclFast2.default.lowlevel.crypto_sign_PUBLICKEYBYTES)
  );
  var secretKey = _convert2.default.hex2uaReversed(this.privateKey);
  _naclFast2.default.lowlevel.crypto_sign_keypair_hash(
    publicKey.data,
    secretKey,
    _utils2.default.hashfunc
  );
  var kp = { secretKey: secretKey, publicKey: publicKey };

  var entity = _utils2.default.prepare(
    publicKey,
    transferTransaction,
    networkId
  );
  entity.timeStamp = entity.timeStamp - 1000;
  var result = _serialization2.default.serializeTransaction(entity);
  var signature = _utils2.default.sign(result, kp);
  var obj = {
    data: _convert2.default.ua2hex(result),
    signature: signature.toString(),
  };

  return obj;
};

var getBalance = async function getBalance(address) {
  address = addressClean(address);
  var response = await _axios2.default.get(
    this.endpoint + ":" + this.defaultPort + "/account/get",
    {
      params: {
        address: address,
      },
    }
  );
  return response.data.account.balance;
};

var getTransactions = async function getTransactions(address) {
  address = addressClean(address);
  var response = await _axios2.default.get(
    this.endpoint + ":" + this.defaultPort + "/account/transfers/all",
    {
      params: {
        address: address,
      },
    }
  );
  return response.data.data;
};

var setPrivateKey = function setPrivateKey(privateKey) {
  this.privateKey = privateKey;
};

var setEndpoint = function setEndpoint(endpoint, port) {
  this.endpoint = endpoint;
  this.defaultPort = 7890;
  this.websocketPort = 7778;
};

var getAddress = function getAddress(networkId) {
  var publicKey = new _utils2.default.BinaryKey(
    new Uint8Array(_naclFast2.default.lowlevel.crypto_sign_PUBLICKEYBYTES)
  );
  var secretKey = _convert2.default.hex2uaReversed(this.privateKey);
  _naclFast2.default.lowlevel.crypto_sign_keypair_hash(
    publicKey.data,
    secretKey,
    _utils2.default.hashfunc
  );

  var binPubKey = _cryptoJs2.default.enc.Hex.parse(publicKey.toString());
  var hash = _cryptoJs2.default.SHA3(binPubKey, {
    outputLength: 256,
  });
  var hash2 = _cryptoJs2.default.RIPEMD160(hash);
  // 98 is for testnet
  var networkPrefix = _utils2.default.id2Prefix(networkId);
  var versionPrefixedRipemd160Hash =
    networkPrefix + _cryptoJs2.default.enc.Hex.stringify(hash2);
  var tempHash = _cryptoJs2.default.SHA3(
    _cryptoJs2.default.enc.Hex.parse(versionPrefixedRipemd160Hash),
    {
      outputLength: 256,
    }
  );
  var stepThreeChecksum = _cryptoJs2.default.enc.Hex.stringify(tempHash).substr(
    0,
    8
  );
  var concatStepThreeAndStepSix = _convert2.default.hex2a(
    versionPrefixedRipemd160Hash + stepThreeChecksum
  );
  var ret = _utils2.default.b32encode(concatStepThreeAndStepSix);
  return ret;
};

var isFromNetwork = function isFromNetwork(_address, networkId) {
  var address = _address.toString().toUpperCase().replace(/-/g, "");
  var a = address[0];
  return _utils2.default.id2Char(networkId) === a;
};

var isAddressValid = function isAddressValid(_address) {
  var address = _address.toString().toUpperCase().replace(/-/g, "");
  if (!address || address.length !== 40) {
    return false;
  }
  var decoded = _convert2.default.ua2hex(_utils2.default.b32decode(address));
  var versionPrefixedRipemd160Hash = _cryptoJs2.default.enc.Hex.parse(
    decoded.slice(0, 42)
  );
  var tempHash = _cryptoJs2.default.SHA3(versionPrefixedRipemd160Hash, {
    outputLength: 256,
  });
  var stepThreeChecksum = _cryptoJs2.default.enc.Hex.stringify(tempHash).substr(
    0,
    8
  );

  return stepThreeChecksum === decoded.slice(42);
};

var addressClean = function addressClean(_address) {
  return _address.toUpperCase().replace(/-|\s/g, "");
};

module.exports = {
  getBalance: getBalance,
  getTransactions: getTransactions,
  send: send,
  setPrivateKey: setPrivateKey,
  getAddress: getAddress,
  setEndpoint: setEndpoint,
  isFromNetwork: isFromNetwork,
  isAddressValid: isAddressValid,
  addressClean: addressClean,
};

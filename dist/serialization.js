'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _convert = require('./convert');

var _convert2 = _interopRequireDefault(_convert);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  serializeTransaction: function serializeTransaction(entity) {
    var r = new ArrayBuffer(512 + 2764);
    var d = new Uint32Array(r);
    var b = new Uint8Array(r);
    d[0] = entity['type'];
    d[1] = entity['version'];
    d[2] = entity['timeStamp'];

    var temp = _convert2.default.hex2ua(entity['signer']);
    d[3] = temp.length;
    var e = 16;
    for (var j = 0; j < temp.length; ++j) {
      b[e++] = temp[j];
    }

    // Transaction
    var i = e / 4;
    d[i++] = entity['fee'];
    d[i++] = Math.floor(entity['fee'] / 0x100000000);
    d[i++] = entity['deadline'];
    e += 12;

    // TransferTransaction
    d[i++] = entity['recipient'].length;
    e += 4;
    // TODO: check that entity['recipient'].length is always 40 bytes
    for (j = 0; j < entity['recipient'].length; ++j) {
      b[e++] = entity['recipient'].charCodeAt(j);
    }
    i = e / 4;
    d[i++] = entity['amount'];
    d[i++] = Math.floor(entity['amount'] / 0x100000000);
    e += 8;

    if (entity['message']['type'] === 1 || entity['message']['type'] === 2) {
      temp = _convert2.default.hex2ua(entity['message']['payload']);
      if (temp.length === 0) {
        d[i++] = 0;
        e += 4;
      } else {
        // length of a message object
        d[i++] = 8 + temp.length;
        // object itself
        d[i++] = entity['message']['type'];
        d[i++] = temp.length;
        e += 12;
        for (j = 0; j < temp.length; ++j) {
          b[e++] = temp[j];
        }
      }
    }

    return new Uint8Array(r, 0, e);
  }
};